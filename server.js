const fs = require('fs');
const path = require('path');

const express = require('express');
const request = require('request');

const app = express();
const port = process.env.PORT || 5000;

app.use(express.static(path.resolve(__dirname, 'public')));

app.get('/protext/:inputText', (req, resp) => {
  const inputText = req.params.inputText;

  // fetch remote asset
  let remoteUrl;
  if (!!(inputText)) {
    remoteUrl = `https://protext.herokuapp.com/?text=${inputText}`;
  } else {
    remoteUrl = `https://protext.herokuapp.com/`;
  }

  const remoteReq = request(remoteUrl);

  // respond with remote asset
  remoteReq.pipe(resp);
});

app.get('*', (req, resp) => {
  const remoteUrl = `https://protext.herokuapp.com${req.url}`;
  console.log(remoteUrl);

  // fetch remote asset
  const remoteReq = request({
    url: remoteUrl,
    headers: {
      // 'Host': 'protext.herokuapp.com',
      // 'Connection': 'keep-alive',
      // 'Origin': 'https://protext.herokuapp.com',
      // 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36',
      // 'Accept': '*/*',
      // 'DNT': '1',
      // 'Referer': 'https://protext.herokuapp.com/',
      // 'Accept-Encoding': 'gzip, deflate',
      // 'Accept-Language': 'en-GB,en;q=0.8',
      'Cookie': 'X-VALID=TRUE',
    },
  });

  // respond with remote asset
  remoteReq.pipe(resp);
});

app.listen(port, () => {
  console.log('Node app is running on port', port);
});
