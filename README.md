# protext-decoder

## Getting Started

1. Clone the repo or [download the source](https://github.com/zhirzh/protext-decoder/archive/master.zip).
2. Install the dependencies with `npm install`.
3. Start with `npm start`.

## How ProText works

ProText (not to be confused with [ProTeXt](https://en.wikipedia.org/wiki/ProTeXt)) works on the same principle of GlyphIcons (symbol fonts). In modern browsers, any printable character can be forced to look a certain way by modifying its `font-face`, via [CSS](https://developer.mozilla.org/en/docs/Web/CSS/@font-face).

ProText uses this property to create a map between standard character set and non-standard character set. (By *standard characters* I mean the `A-Z, a-z, 0-9, SPACE` character set and `non-standard characters` are all printable characters, except those in the *standard set*.)

To alter the representation, a font-sheet is generated that maps the non-standard characters (only those used in the text) to [glyphs](https://en.wikipedia.org/wiki/Glyph) that look like the related standard set.

The said font-sheet is served to the browser, along with the cipher text (text written in non-standard set) and CSS rules that control the representation.

## How ProText Decoder works

Once you know the trick employed, it becomes fairly easy to out-trick it :sunglasses:.

One of the two main components of the decoder is [opentype.js](https://github.com/nodebox/opentype.js) - JavaScript font parser. Load the font-sheet in `opentype.js` to get the mappings between characters and their glyphs.

Since the characters in glyphs are stored as `unicode` values, we need to convert HTML entities to their unicode equivalents, find the corresponding glyph and then decode the text ine character at a time. Another, much simpler method is to simply [render the text onto canvas](https://github.com/nodebox/opentype.js#fontdrawctx-text-x-y-fontsize-options).
